#include "gtest/gtest.h"

#include "PackageQueue.hpp"

TEST(PackageQueueTest, TakingPackagesTest) {
    PackageQueue test_LIFO(QueueType::LIFO);
    PackageQueue test_FIFO(QueueType::FIFO);
    Package package1;
    Package package2;
    Package package3;
    Package package4;
    test_FIFO.add_package(package1);
    test_FIFO.add_package(package2);
    test_LIFO.add_package(package3);
    test_LIFO.add_package(package4);
    Package package_LIFO=test_LIFO.take_package();
    Package package_FIFO=test_FIFO.take_package();
    EXPECT_EQ(package_FIFO.get_id(),package1.get_id());
    EXPECT_EQ(package_LIFO.get_id(), package4.get_id());
}