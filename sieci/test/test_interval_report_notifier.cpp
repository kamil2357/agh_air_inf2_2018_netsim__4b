// 4b: Wrześniak (297474), Twardosz (297466)

#include "gtest/gtest.h"

#include "IntervalReportNotifier.hpp"

TEST(IntervalReportNotifier, reportingTest) {
    IntervalReportNotifier notifier(4);
    EXPECT_EQ(notifier.should_generate_report(1), true);
    EXPECT_EQ(notifier.should_generate_report(5), true);
    EXPECT_EQ(notifier.should_generate_report(4), false);
}

// 4b: Wrześniak (297474), Twardosz (297466)