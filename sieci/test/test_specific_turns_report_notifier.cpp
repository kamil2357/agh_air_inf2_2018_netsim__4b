// 4b: Wrześniak (297474), Twardosz (297466)

#include "gtest/gtest.h"

#include "SpecificTurnsReportNotifier.hpp"

TEST(SpecificTurnsReportNotifier, reportingTest) {
    SpecificTurnsReportNotifier notifier({1,3,7});
    EXPECT_EQ(notifier.should_generate_report(1), true);
    EXPECT_EQ(notifier.should_generate_report(7), true);
    EXPECT_EQ(notifier.should_generate_report(4), false);
}

// 4b: Wrześniak (297474), Twardosz (297466)