// 4b: Wrześniak (297474), Twardosz (297466)

#include "gtest/gtest.h"

#include "NodeCollection.hpp"

TEST(NodeCollectionTest, addingOrderTest) {
    Ramps ramps;

    ramps.add_node({1,1});
    ramps.add_node({3,1});
    ramps.add_node({2, 1});

    ElementID expected_id = 1;
    for(const auto& elem : ramps) {
        EXPECT_EQ(elem.get_id(), expected_id++);
    }
}

TEST(NodeCollectionTest, searchingTest) {
    Ramps ramps;

    ramps.add_node({77,1});
    ramps.add_node({13,1});


    EXPECT_EQ(ramps.get_element(77).get_id(), 77);
    EXPECT_EQ(ramps.get_element(13).get_id(), 13);
}

TEST(NodeCollectionTest, removingTest) {
    Ramps ramps;

    ramps.add_node({77,1});
    ramps.add_node({13,1});

    ramps.remove_node(13);

    const auto& elems = ramps.get_elements();
    ASSERT_EQ(elems.size(), 1);
    ASSERT_EQ(elems.front().get_id(), 77);
}

// 4b: Wrześniak (297474), Twardosz (297466)