#include <PackageQueue.hpp>
#include "gtest/gtest.h"

#include "Storehouse.hpp"

TEST(PackageSenderTest, EmptyTest) {
    Storehouse test(1,std::make_unique<PackageQueue>(PackageQueue(QueueType::FIFO)));
    Storehouse test2(2,std::make_unique<PackageQueue>(PackageQueue(QueueType::LIFO)));
    Package package1;
    Package package2;
    Package package3;
    Package package4;
    test.take_package(package1);
    test.take_package(package2);
    test2.take_package(package3);
    test2.take_package(package4);
    std::vector<Package> package_LIFO=test2.get_packages();
    std::vector<Package> package_FIFO=test.get_packages();
    EXPECT_EQ(package_FIFO[0].get_id()==package1.get_id(),1);
    EXPECT_EQ(package_LIFO[0].get_id()==package4.get_id(),1);
}