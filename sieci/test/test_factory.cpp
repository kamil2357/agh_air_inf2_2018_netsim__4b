// 4b: Wrześniak (297474), Twardosz (297466)

#include "gtest/gtest.h"

#include "Factory.hpp"
#include "iofunctions.hpp"

TEST(Factory, removingTest) {
    Factory factory;
    factory.add_ramp({1,1});
    factory.add_worker({1,1,std::make_unique<PackageQueue>(QueueType::LIFO)});
    factory.add_worker({2,1,std::make_unique<PackageQueue>(QueueType::LIFO)});
    factory.add_storehouse({1,std::make_unique<PackageQueue>(QueueType::LIFO)});

    factory.create_ramp_worker_link(1,1);
    factory.create_worker_worker_link(2,1);
    factory.create_worker_storehouse_link(1,1);

    Worker& w1 = factory.get_worker(1);
    ASSERT_TRUE(factory.get_ramp(1).is_receiver(&w1));
    ASSERT_TRUE(factory.get_worker(2).is_receiver(&w1));

    factory.remove_worker(1);
    EXPECT_FALSE(factory.get_ramp(1).is_receiver(&w1));
    EXPECT_FALSE(factory.get_worker(2).is_receiver(&w1));
}

TEST(Factory, unconnectedRampTest) {
    Factory factory;
    factory.add_ramp({1,1});
    factory.add_ramp({2,1});
    factory.add_worker({1,1,std::make_unique<PackageQueue>(QueueType::LIFO)});
    factory.add_worker({2,1,std::make_unique<PackageQueue>(QueueType::LIFO)});
    factory.add_storehouse({1,std::make_unique<PackageQueue>(QueueType::LIFO)});
    factory.create_ramp_worker_link(1,1);
    factory.create_ramp_worker_link(1,2);
    factory.create_worker_worker_link(1,2);
    factory.create_worker_storehouse_link(2,1);
    EXPECT_THROW(factory.check_structure(), std::runtime_error);
}

TEST(Factory, unconnectedWorkerTest) {
    Factory factory;
    factory.add_ramp({1,1});
    factory.add_worker({1,1,std::make_unique<PackageQueue>(QueueType::LIFO)});
    factory.add_worker({2,1,std::make_unique<PackageQueue>(QueueType::LIFO)});
    factory.add_worker({3,1,std::make_unique<PackageQueue>(QueueType::LIFO)});
    factory.add_storehouse({1,std::make_unique<PackageQueue>(QueueType::LIFO)});
    factory.create_ramp_worker_link(1,1);
    factory.create_ramp_worker_link(1,2);
    factory.create_worker_worker_link(1,2);
    factory.create_worker_worker_link(2,3);
    factory.create_worker_worker_link(3,2);
    factory.create_worker_storehouse_link(1,1);
    EXPECT_THROW(factory.check_structure(), std::runtime_error);
}

TEST(Factory, correctStructureTest) {
    Factory factory;
    factory.add_ramp({1,1});
    factory.add_ramp({2,1});
    factory.add_worker({1,1,std::make_unique<PackageQueue>(QueueType::LIFO)});
    factory.add_worker({2,1,std::make_unique<PackageQueue>(QueueType::LIFO)});
    factory.add_storehouse({1,std::make_unique<PackageQueue>(QueueType::LIFO)});
    factory.create_ramp_worker_link(1,1);
    factory.create_ramp_worker_link(2,1);
    factory.create_worker_worker_link(1,2);
    factory.create_worker_storehouse_link(2,1);
    EXPECT_NO_THROW(factory.check_structure());
}

TEST(Factory, simulationTest) {
    Factory factory;
    factory.add_ramp({1,1});
    factory.add_ramp({2,1});
    factory.add_worker({1,2,std::make_unique<PackageQueue>(QueueType::FIFO)});
    factory.add_worker({2,2,std::make_unique<PackageQueue>(QueueType::FIFO)});
    factory.add_storehouse({1, std::make_unique<PackageQueue>(QueueType::FIFO)});
    factory.create_ramp_worker_link(1,1);
    factory.create_ramp_worker_link(2,2);
    factory.create_worker_storehouse_link(2,1);
    factory.create_worker_storehouse_link(1,1);
    //pierwsza tura symulacji
    Package::reset_packaged_id();
    factory.update();

    //pierwszy pracownik
    EXPECT_EQ(factory.get_worker(1).get_processed_package().get_id(), 1);
    EXPECT_TRUE(factory.get_worker(1).get_packages().empty());

    //drugin pracownik
    EXPECT_EQ(factory.get_worker(2).get_processed_package().get_id(), 2);
    EXPECT_TRUE(factory.get_worker(2).get_packages().empty());
}

// 4b: Wrześniak (297474), Twardosz (297466)