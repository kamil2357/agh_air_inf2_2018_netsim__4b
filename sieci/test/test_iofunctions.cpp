// 4b: Wrześniak (297474), Twardosz (297466)

#include "gtest/gtest.h"
#include "iofunctions.hpp"
#include "IntervalReportNotifier.hpp"

TEST(iofunction, loadingFactoryStructureTest){
    std::ifstream in("../test/factory_structures/factory_structure.txt");
    Factory factory = load_factory_structure(in);
    //poprawne wczytanie rampy
    ASSERT_EQ(factory.get_ramps().size(), 1);
    EXPECT_EQ(factory.get_ramps().front().get_id(), 1);
    //poprawne wczytanie pracownika
    ASSERT_EQ(factory.get_workers().size(), 1);
    EXPECT_EQ(factory.get_workers().front().get_id(), 1);
    //poprawne wczytanie magazynu
    ASSERT_EQ(factory.get_storehouses().size(), 1);
    EXPECT_EQ(factory.get_storehouses().front().get_id(), 1);
    //sprawdzenie polaczen rampy
    ASSERT_EQ(factory.get_ramp(1).get_receivers().size(), 1);
    EXPECT_TRUE(factory.get_ramp(1).is_receiver(&factory.get_worker(1)));
    //sprawdzenie polaczen pracownika
    ASSERT_EQ(factory.get_worker(1).get_receivers().size(), 1);
    EXPECT_TRUE(factory.get_worker(1).is_receiver(&factory.get_storehouse(1)));
}

TEST(iofunction, savingFactoryStructureTest){
    Factory factory;
    factory.add_ramp({1,2});
    factory.add_worker({1,2,std::make_unique<PackageQueue>(QueueType::FIFO)});
    factory.add_storehouse({1, std::make_unique<PackageQueue>(QueueType::FIFO)});
    factory.create_ramp_worker_link(1,1);
    factory.create_worker_storehouse_link(1,1);
    std::ofstream out("../test/factory_structures/factory_structure_out.txt");
    save_factory_structure(out, factory);
    out.close();
    //sprawdzenie czy odczytana fabryka jest taka sama
    std::ifstream in("../test/factory_structures/factory_structure_out.txt");
    Factory loaded_factory = load_factory_structure(in);
    //poprawne wczytanie rampy
    ASSERT_EQ(loaded_factory.get_ramps().size(), 1);
    EXPECT_EQ(loaded_factory.get_ramps().front().get_id(), 1);
    //poprawne wczytanie pracownika
    ASSERT_EQ(loaded_factory.get_workers().size(), 1);
    EXPECT_EQ(loaded_factory.get_workers().front().get_id(), 1);
    //poprawne wczytanie magazynu
    ASSERT_EQ(loaded_factory.get_storehouses().size(), 1);
    EXPECT_EQ(loaded_factory.get_storehouses().front().get_id(), 1);
    //sprawdzenie polaczen rampy
    ASSERT_EQ(loaded_factory.get_ramp(1).get_receivers().size(), 1);
    EXPECT_TRUE(loaded_factory.get_ramp(1).is_receiver(&loaded_factory.get_worker(1)));
    //sprawdzenie polaczen pracownika
    ASSERT_EQ(loaded_factory.get_worker(1).get_receivers().size(), 1);
    EXPECT_TRUE(loaded_factory.get_worker(1).is_receiver(&loaded_factory.get_storehouse(1)));
}

TEST(iofunction, structureRaportTest) {
    Factory factory;
    factory.add_ramp({1,2});
    factory.add_worker({1,2,std::make_unique<PackageQueue>(QueueType::FIFO)});
    factory.add_storehouse({1, std::make_unique<PackageQueue>(QueueType::FIFO)});
    factory.create_ramp_worker_link(1,1);
    factory.create_worker_storehouse_link(1,1);
    //======================================================
    const std::string expected_raport =
            "\n"
            "== LOADING_RAMPS ==\n"
            "\n"
            "LOADING_RAMP #1\n"
            "  Delivery interval: 2\n"
            "  Receivers:\n"
            "    worker #1\n"
            "\n"
            "\n"
            "== WORKERS ==\n"
            "\n"
            "WORKER #1\n"
            "  Processing time: 2\n"
            "  Queue type: FIFO\n"
            "  Receivers:\n"
            "    storehouse #1\n"
            "\n"
            "\n"
            "== STOREHOUSES ==\n"
            "\n"
            "STOREHOUSE #1\n"
            "\n";
    //======================================================
    EXPECT_EQ(generate_structure_report(factory), expected_raport);
}

TEST(iofunction, structureTurnRaportTest) {
    Factory factory;
    factory.add_ramp({1,1});
    factory.add_ramp({2,1});
    factory.add_worker({1,2,std::make_unique<PackageQueue>(QueueType::FIFO)});
    factory.add_worker({2,2,std::make_unique<PackageQueue>(QueueType::FIFO)});
    factory.add_storehouse({1, std::make_unique<PackageQueue>(QueueType::FIFO)});
    factory.create_ramp_worker_link(1,1);
    factory.create_ramp_worker_link(2,2);
    factory.create_worker_storehouse_link(2,1);
    factory.create_worker_storehouse_link(1,1);
    //pierwsza tura symulacji
    Package::reset_packaged_id();
    factory.update();
    //======================================================
    const std::string expected_raport =
            "\n"
            "== WORKERS ==\n"
            "\n"
            "WORKER #1\n"
            "  Queue: #1 (pt = 1)\n"
            "\n"
            "WORKER #2\n"
            "  Queue: #2 (pt = 1)\n"
            "\n"
            "\n"
            "== STOREHOUSES ==\n"
            "\n"
            "STOREHOUSE #1\n"
            "  Queue:\n"
            "\n";
    //======================================================
    EXPECT_EQ(genertate_simulation_turn_report(factory), expected_raport);
}

// 4b: Wrześniak (297474), Twardosz (297466)