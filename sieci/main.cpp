#include "Factory.hpp"
#include "IntervalReportNotifier.hpp"
#include "IPackageQueue.hpp"
#include "IPackageReceiver.hpp"
#include "IReportNotifier.hpp"
#include "NodeCollection.hpp"
#include "Package.hpp"
#include "PackageQueue.hpp"
#include "PackageSender.hpp"
#include "Ramp.hpp"
#include "ReceiverPreferences.hpp"
#include "SpecificTurnsReportNotifier.hpp"
#include "Storehouse.hpp"
#include "Types.hpp"
#include "Worker.hpp"

int main() {
    return 0;
}