#include "ReceiverPreferences.hpp"

ProbabilityGenerator DEFAULT_PROBABILITY_GENERATOR=[](){
    static std::mt19937 rng(std::random_device{}());
    static std::uniform_real_distribution<double> dist(0,1);
    return dist(rng);
};

void ReceiverPreferences::add_receiver(IPackageReceiver *p_receiver) {
    _receiver_preferences[p_receiver]=1.0;
    probability_scale();
}

std::vector<IPackageReceiver*> ReceiverPreferences::get_receivers() const {
    std::vector<IPackageReceiver*> out;
    for(auto elem:_receiver_preferences){
        out.push_back(elem.first);
    }
    return out;
}

void ReceiverPreferences::remove_receiver(IPackageReceiver *p_receiver) {
    auto it=_receiver_preferences.find(p_receiver);
    _receiver_preferences.erase(it);
    probability_scale();
}

bool ReceiverPreferences::contains(IPackageReceiver* p_receiver)const{
    return _receiver_preferences.find(p_receiver)!=_receiver_preferences.end();
}

void ReceiverPreferences::probability_scale() {
    if (!_receiver_preferences.empty()) {
        double new_probability = 1.0 / (double(_receiver_preferences.size()));
        for (auto &element:_receiver_preferences) {
            element.second = new_probability;
        }
    }
}

IPackageReceiver* ReceiverPreferences::ger_rand_receiver()const{
    double random_num=_generator();
    double sum=0.0;
    for(auto elem:_receiver_preferences){
        sum+=elem.second;
        if (sum>=random_num){
            return elem.first;
        }
    }
    //w przypadku kumulowania sie bledow zaokraglen suma prawdopodobienstw moze przyjac wartosc mniejsza niz 1
    return !_receiver_preferences.empty() ? _receiver_preferences.begin()->first:nullptr;
}