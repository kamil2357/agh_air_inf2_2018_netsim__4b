#include "PackageQueue.hpp"

#include <algorithm>


void PackageQueue::add_package(Package package) {
    if (_queue_type==QueueType::FIFO){
        _package_col.push_back(package);
    }
    else if (_queue_type==QueueType::LIFO){
        _package_col.push_front(package);
    }
}

std::vector<Package> PackageQueue::get_packages() const {
    std::vector<Package> vec;
    for(auto elem:_package_col ){
        vec.push_back(elem);
    }
    return vec;
}

void PackageQueue::remove_package(ElementID id) {
    _package_col.erase(std::remove_if(_package_col.begin(), _package_col.end(),
            [id](Package package) { return package.get_id() == id; }), _package_col.end());
}

bool PackageQueue::is_empty() const {
    return _package_col.empty();
}

Package PackageQueue::take_package() {
    Package package=_package_col.front();
    _package_col.pop_front();
    return package;
}

QueueType PackageQueue::get_queue_type() const {
    return _queue_type;
}

std::unique_ptr<IPackageQueue> PackageQueue::clone() const {
    return std::make_unique<PackageQueue>(*this);
}

std::unique_ptr<IPackageStockpile> PackageQueue::clone_storehouse()const {
    return std::make_unique<PackageQueue>(*this);
}