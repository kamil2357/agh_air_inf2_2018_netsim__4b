// 4b: Wrześniak (297474), Twardosz (297466)
#include <Worker.hpp>

#include "Worker.hpp"

void Worker::process_package() {
    if (_processed_package){
        _processing_progress++;
    }
}

void Worker::send_if_finished() {
    if(_processing_progress==_period){
        _processing_progress=0;
        const Package to_send=*_processed_package;
        _processed_package.reset();
        send_package(to_send);
    }
}

void Worker::process_new_package() {
    if (!_processed_package && !_packages->is_empty()){
        _processed_package=_packages->take_package();
    }

}

ElementID Worker::get_id() const {
    return _id;
}

void Worker::take_package(Package package){
    _packages->add_package(package);
}

ReceiverType Worker::get_receiver_type()const{
    return _receiver_type;
}

std::vector<Package> Worker::get_packages()const{
    return _packages->get_packages();
}

Time Worker::get_period() const {
    return _period;
}

QueueType Worker::get_queue_type() const{
    return _packages->get_queue_type();
}

Package Worker::get_processed_package() const {
    return _processed_package.value();
}

TimeOffset Worker::get_processing_progress() const {
    return _processing_progress;
}
// 4b: Wrześniak (297474), Twardosz (297466)