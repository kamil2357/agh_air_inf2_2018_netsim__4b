#include "PackageSender.hpp"

void PackageSender::add_receiver(IPackageReceiver *p_receiver) {
    _receiver_preferences.add_receiver(p_receiver);
}

void PackageSender::remove_receiver(IPackageReceiver* p_receiver){
    _receiver_preferences.remove_receiver(p_receiver);
}

std::vector<IPackageReceiver*> PackageSender::get_receivers()const{
    return _receiver_preferences.get_receivers();
}

bool PackageSender::is_receiver(IPackageReceiver* p_receiver)const{
    return _receiver_preferences.contains(p_receiver);
}

void PackageSender::send_package(Package package){
    IPackageReceiver* p_receiver=_receiver_preferences.ger_rand_receiver();
    p_receiver->take_package(package);
}