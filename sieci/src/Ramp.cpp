// 4b: Wrześniak (297474), Twardosz (297466)
#include "Ramp.hpp"

void Ramp::delivery_processing() {
    _from_last_delivery++;
    if(_from_last_delivery==_period){
        _from_last_delivery=0;
        _hold_package=Package();
    }
}

void Ramp::give_if_finished() {
    if(_hold_package){
        const Package to_give=*_hold_package;
        _hold_package.reset();
        send_package(to_give);
}
}
ElementID Ramp::get_id() const {
    return _id;
}

Time Ramp::get_period() const {
    return _period;
}

// 4b: Wrześniak (297474), Twardosz (297466)