#include "Storehouse.hpp"

void Storehouse::take_package(Package package) {
    _products->add_package(package);
}

std::vector<Package> Storehouse::get_packages()const{
    return _products->get_packages();
}

ReceiverType Storehouse::get_receiver_type() const {
    return _receiver_type;
}

ElementID Storehouse::get_id() const {
    return _id;
}