// 4b: Wrześniak (297474), Twardosz (297466)

#include "Factory.hpp"
#include "iofunctions.hpp"

#include <string>
#include <sstream>
#include <Factory.hpp>


void Factory::update(){
    //dostawa
    for(auto& ramp : _ramps){
        ramp.delivery_processing();
    }
    //przekazywanie
    for(auto& ramp : _ramps){
        ramp.give_if_finished();
    }
    for(auto& worker : _workers){
        worker.send_if_finished();
    }
    for(auto& worker : _workers){
        worker.process_new_package();
    }
    //przetworzenie
    for(auto& worker : _workers){
        worker.process_package();
    }
}

void Factory::add_ramp(Ramp ramp){
    _ramps.add_node(std::move(ramp));
}

void Factory::add_worker(Worker worker){
    _workers.add_node(std::move(worker));
}

void Factory::add_storehouse(Storehouse storehouse){
    _storehouses.add_node(std::move(storehouse));
}

Ramp& Factory::get_ramp(ElementID id){
    return _ramps.get_element(id);
}

Worker& Factory::get_worker(ElementID id){
    return _workers.get_element(id);
}

Storehouse& Factory::get_storehouse(ElementID id){
    return _storehouses.get_element(id);
}

void Factory::create_ramp_worker_link(ElementID ramp_id, ElementID worker_id){
    Ramp& ramp = get_ramp(ramp_id);
    Worker& worker = get_worker(worker_id);
    if(ramp.is_receiver(&worker)){
        throw std::runtime_error("Polaczenie miedzy rama o id: " + std::to_string(ramp_id) +
        " a pracownikiem o id: " + std::to_string(worker_id) + " juz istnieje");
    }
    ramp.add_receiver(&worker);
}

void Factory::create_worker_worker_link(ElementID worker1_id, ElementID worker2_id){
    Worker& worker1 = get_worker(worker1_id);
    Worker& worker2 = get_worker(worker2_id);
    if(worker1.is_receiver(&worker2)){
        throw std::runtime_error("Polaczenie miedzy pracownikiem o id: " + std::to_string(worker1_id) +
        " a pracownikiem o id: " + std::to_string(worker2_id) + " juz istnieje");
    }
    worker1.add_receiver(&worker2);
}

void Factory::create_worker_storehouse_link(ElementID worker_id, ElementID storehose_id) {
    Worker& worker = get_worker(worker_id);
    Storehouse& storehouse = get_storehouse(storehose_id);
    if(worker.is_receiver(&storehouse)){
        throw std::runtime_error("Polaczenie miedzy pracownikiem o id: " + std::to_string(worker_id) +
        " a magazynem o id: " + std::to_string(storehose_id) + " juz istnieje");
    }
    worker.add_receiver(&storehouse);
}

void Factory::remove_ramp(ElementID id) {
    _ramps.remove_node(id);
}

void Factory::remove_worker(ElementID id) {
    Worker& worker_to_remove = get_worker(id);
    for(auto& ramp: _ramps){
        if(ramp.is_receiver(&worker_to_remove)){
            ramp.remove_receiver(&worker_to_remove);
        }
    }
    for(auto& worker: _workers){
        if(worker.is_receiver(&worker_to_remove)){
            worker.remove_receiver(&worker_to_remove);
        }
    }
    _workers.remove_node(id);
}

void Factory::remove_storehouse(ElementID id) {
    Storehouse& storehouse_to_remove = get_storehouse(id);
    for(auto& worker: _workers){
        if(worker.is_receiver(&storehouse_to_remove)){
            worker.remove_receiver(&storehouse_to_remove);
        }
    }
    _storehouses.remove_node(id);
}

void Factory::check_structure() const {
    struct WorkerNode{
        std::vector<ElementID> src;
        std::vector<ElementID> dest;
        bool rechable = false;
        bool valid = false;
    };

    for(const auto& ramp : _ramps){
        if(ramp.get_receivers().empty()){
            throw std::runtime_error("Rampa o id: " + std::to_string(ramp.get_id())
            + " nie jest polaczona z zadnym robotnikiem");
        }
    }

    std::map<ElementID, WorkerNode> worker_nodes;

    for(const auto& worker : _workers){
        worker_nodes[worker.get_id()] = WorkerNode();
    }

    for(const auto& worker : _workers){
        for(auto p_receiver : worker.get_receivers()){
            switch(p_receiver->get_receiver_type()){
                case ReceiverType::Worker:
                    worker_nodes[p_receiver->get_id()].src.push_back(worker.get_id());
                    worker_nodes[worker.get_id()].dest.push_back(p_receiver->get_id());
                    break;
                case ReceiverType::Storehouse:
                    worker_nodes[worker.get_id()].valid = true;
                    break;
            }
        }
    }

    for(const auto& ramp : _ramps){
        for(auto p_receiver : ramp.get_receivers()){
            worker_nodes[p_receiver->get_id()].rechable = true;
        }
    }

    std::function<void(ElementID)> set_rechable = [&](ElementID worker_id){
        worker_nodes[worker_id].rechable = true;
        for(ElementID dest_id : worker_nodes[worker_id].dest){
            if(!worker_nodes[dest_id].rechable){
                set_rechable(dest_id);
            }
        }
    };

    std::vector<ElementID> starting_workers_id;
    for(const auto& elem : worker_nodes){
        if(elem.second.rechable) {
            starting_workers_id.push_back(elem.first);
        }
    }
    for(const auto& worker_id : starting_workers_id){
        for(ElementID dest_id : worker_nodes[worker_id].dest){
            if(!worker_nodes[dest_id].rechable){
                set_rechable(dest_id);
            }
        }
    }

    std::function<void(ElementID)> set_valid = [&](ElementID worker_id){
        worker_nodes[worker_id].valid = true;
        for(ElementID src_id : worker_nodes[worker_id].src){
            if(worker_nodes[src_id].rechable && !worker_nodes[src_id].valid){
                set_valid(src_id);
            }
        }
    };

    std::vector<ElementID> ending_workers_id;
    for(const auto& elem : worker_nodes){
        if(elem.second.valid) {
            ending_workers_id.push_back(elem.first);
        }
    }

    for(const auto& worker_id : ending_workers_id){
        for(ElementID src_id : worker_nodes[worker_id].src){
            if(worker_nodes[src_id].rechable && !worker_nodes[src_id].valid){
                set_valid(src_id);
            }
        }
    }

    for(const auto& elem : worker_nodes){
        if(elem.second.rechable && !elem.second.valid){
            throw std::runtime_error("Robotnik o id: " + std::to_string(elem.first) +
            " nie jest polaczany z zadnym magazynem");
        }
    }
}

Factory::Factory(const Factory &src)
    :_ramps(src._ramps), _workers(src._workers), _storehouses(src._storehouses){
    for(const auto& ramp : src.get_ramps()){
        for(const auto receiver : ramp.get_receivers()){
            create_ramp_worker_link(ramp.get_id(), receiver->get_id());
        }
    }
    for(const auto& worker : src.get_workers()){
        for(const auto receiver : worker.get_receivers()){
            switch(receiver->get_receiver_type()){
                case ReceiverType::Worker:
                    create_worker_worker_link(worker.get_id(), receiver->get_id());
                    break;
                case ReceiverType::Storehouse:
                    create_worker_storehouse_link(worker.get_id(), receiver->get_id());
                    break;
            }
        }
    }
}

void simulate(Factory factory, Time simulation_time, std::ostream& out,
              std::unique_ptr<IReportNotifier> p_report_notyfier){
    try {
        factory.check_structure();
    }
    catch(const std::runtime_error& e){
        return;
    }
    Package::reset_packaged_id();
    for(Time turn = 1; turn <= simulation_time; turn++){
        factory.update();
        if(p_report_notyfier->should_generate_report(turn)){
            out << genertate_simulation_turn_report(factory);
        }
    }
}


// 4b: Wrześniak (297474), Twardosz (297466)