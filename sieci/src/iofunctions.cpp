// 4b: Wrześniak (297474), Twardosz (297466)

#include "Factory.hpp"
#include "iofunctions.hpp"

#include <map>
#include <sstream>

using Tokens = std::map<std::string, std::string>;

enum class Type{
    LOADING_RAMP,
    WORKER,
    STOREHOUSE,
    LINK
};

std::string to_string(ReceiverType receiver_type){
    std::string out;
    switch(receiver_type){
        case ReceiverType::Worker:
            out = "worker";
            break;
        case ReceiverType::Storehouse:
            out = "store";
            break;
    }
    return out;
}

std::string to_string(QueueType queue_type){
    std::string out;
    switch(queue_type){
        case QueueType::FIFO:
            out = "FIFO";
            break;
        case QueueType::LIFO:
            out = "LIFO";
            break;
    }
    return out;
}

Type str_to_type(const std::string& type){
    if(type == "LOADING_RAMP"){
        return Type::LOADING_RAMP;
    }
    if(type == "WORKER"){
        return Type::WORKER;
    }
    if(type == "STOREHOUSE"){
        return Type::STOREHOUSE;
    }
    if(type == "LINK"){
        return Type::LINK;
    }
    throw std::runtime_error("Prodano bledny identyfikator typu");
}

QueueType str_to_queue_type(const std::string& queue_type){
    if(queue_type == "FIFO"){
        return QueueType::FIFO;
    }
    if(queue_type == "LIFO"){
        return QueueType::LIFO;
    }
    throw std::runtime_error("Podano bledny identyfikator kolejki");
}

Tokens get_tokens(std::string&& line){
    Tokens out;
    std::string token;
    char delimiter = ' ';
    std::istringstream token_stream(line);

    while(std::getline(token_stream, token, delimiter)){
        auto pos = token.find('=');
        out[token.substr(0, pos)] = token.substr(pos+1, token.size() - pos);
    }
    return out;
}

unsigned int str_to_uint(const std::string& str) {
    auto str_it = std::find_if(str.cbegin(), str.cend(), [](char ch) { return !std::isdigit(ch); });
    if (str_it != str.cend()) {
        throw std::runtime_error("Bledna wartosc: " + str);
    }
    return (unsigned int)atoi(str.c_str());
}

Type str_to_src_type(const std::string& str){
    if(str == "ramp"){
        return Type::LOADING_RAMP;
    }
    if(str == "worker"){
        return Type::WORKER;
    }
    throw std::runtime_error(str + " nie jest typem nadawcy");
}

Type str_to_dest_type(const std::string& str){
    if(str == "worker"){
        return Type::WORKER;
    }
    if(str == "store"){
        return Type::STOREHOUSE;
    }
    throw std::runtime_error(str + " nie jest typem odbiorcy");
}

std::pair<Type, ElementID> str_to_src(const std::string& str){
    const auto pos = str.find('-');
    const std::string type_part = str.substr(0, pos);
    const std::string id_part = str.substr(pos + 1, str.size() - pos - 1);
    return {str_to_src_type(type_part), str_to_uint(id_part)};
}

std::pair<Type, ElementID> str_to_dest(const std::string& str){
    const auto pos = str.find('-');
    const std::string type_part = str.substr(0, pos);
    const std::string id_part = str.substr(pos + 1, str.size() - pos - 1);
    return {str_to_dest_type(type_part), str_to_uint(id_part)};
}

std::string get_info_form_tokens(const std::string& elem_name, const Tokens& tokens){
    return tokens.at(elem_name);
}

void process_line(std::string&& line, Type& previous_element_type, Factory& factory){
    if(!line.empty() && line[0] != ';' ){
        std::size_t first_space_pos = line.find(' ');
        const std::string type_str = line.substr(0, first_space_pos);
        Type type = str_to_type(type_str);
        Tokens tokens = get_tokens(line.substr(first_space_pos + 1, line.size() - first_space_pos - 1));
        switch(type){
            case Type::LOADING_RAMP: {
                const std::string id_str = get_info_form_tokens("id", tokens);
                const std::string interval_str = get_info_form_tokens("delivery-interval", tokens);
                ElementID id = str_to_uint(id_str);
                Time interval = str_to_uint(interval_str);
                if (tokens.size() > 2) {
                    throw std::runtime_error("Podano zbyt duza liczbe danych");
                }
                if(previous_element_type != Type::LOADING_RAMP){
                    throw std::runtime_error("Rampa nie moze wystepowac po innym elemencie niz rampa");
                }
                factory.add_ramp({id, interval});
                break;
            }
            case Type::WORKER: {
                const std::string id_str = get_info_form_tokens("id", tokens);
                const std::string processing_time_str = get_info_form_tokens("processing-time", tokens);
                const std::string queue_type_str = get_info_form_tokens("queue-type", tokens);
                ElementID id = str_to_uint(id_str);
                Time processing_time =str_to_uint(processing_time_str);
                QueueType queue_type = str_to_queue_type(queue_type_str);
                if (tokens.size() > 3) {
                    throw std::runtime_error("Podano zbyt duza liczbe danych");
                }
                if(previous_element_type != Type::LOADING_RAMP && previous_element_type != Type::WORKER){
                    throw std::runtime_error("Pracownik musi wystepowac po pracowniku lub rampie");
                }
                factory.add_worker({id, processing_time, std::make_unique<PackageQueue>(queue_type)});
                previous_element_type = Type::WORKER;
                break;
            }
            case Type::STOREHOUSE: {
                const std::string id_str = get_info_form_tokens("id", tokens);
                auto id = (ElementID) str_to_uint(id_str);
                if (tokens.size() > 1) {
                    throw std::runtime_error("Podano zbyt duza liczbe danych");
                }
                if (previous_element_type == Type::LINK) {
                    throw std::runtime_error("Magazyn nie moze wystepowac po polaczeniu");
                }
                factory.add_storehouse({id, std::make_unique<PackageQueue>(QueueType::FIFO)});
                previous_element_type = Type::STOREHOUSE;
                break;
            }
            case Type::LINK:{
                const std::string src_str = get_info_form_tokens("src", tokens);
                const std::string dest_str = get_info_form_tokens("dest", tokens);
                const auto src = str_to_src(src_str);
                const auto dest = str_to_dest(dest_str);
                if (tokens.size() > 2) {
                    throw std::runtime_error("Podano zbyt duza liczbe danych");
                }
                if(src.first == Type::LOADING_RAMP){
                    if(dest.first == Type::WORKER){
                        factory.create_ramp_worker_link(src.second, dest.second);
                    }
                    else{
                        throw("Nie mozna polaczyc rampy z magazynem");
                    }
                }
                else{
                    if(dest.first == Type::WORKER){
                        factory.create_worker_worker_link(src.second, dest.second);
                    }
                    else{
                        factory.create_worker_storehouse_link(src.second, dest.second);
                    }
                }
                previous_element_type = Type::LINK;
                break;
            }
        }
    }
}

Factory load_factory_structure(std::istream& in){
    Factory out;
    std::string line;
    Type previous_element_type = Type::LOADING_RAMP;
    try {
        while (std::getline(in, line)) {
            process_line(std::move(line), previous_element_type, out);
        }
    }
    catch(...){
        return Factory();
    }
    return out;
}

void save_factory_structure(std::ostream& out, const Factory& factory){
    out << "; == LOADING RAMPS ==" << std::endl;
    out << std::endl;
    for(const auto& ramp : factory.get_ramps()){
        out << "LOADING_RAMP id=" << ramp.get_id() << " delivery-interval=" << ramp.get_period()<<std::endl;
    }
    out << std::endl;
    out << "; == WORKERS ==" << std::endl;
    out << std::endl;
    for(const auto& worker : factory.get_workers()){
        out << "WORKER id=" << worker.get_id() << " processing-time=" << worker.get_period() << " queue-type="
        <<to_string(worker.get_queue_type()) <<std::endl;
    }
    out << std::endl;
    out << "; == STOREHOUSES ==" << std::endl;
    out << std::endl;
    for(const auto& storehouse : factory.get_storehouses()){
        out << "STOREHOUSE id=" << storehouse.get_id() << std::endl;
    }
    out << std::endl;
    out << "; == LINKS ==" << std::endl;
    out << std::endl;
    for(const auto& ramp : factory.get_ramps()){
        for(auto receiver : ramp.get_receivers()){
            out << "LINK src=ramp-" <<  ramp.get_id() << " dest=worker-" << receiver->get_id() <<std::endl;
        }
    }
    for(const auto& worker : factory.get_workers()){
        for(auto receiver : worker.get_receivers()){
            out << "LINK src=worker-" <<  worker.get_id()
            << " dest=" << to_string(receiver->get_receiver_type()) << "-" << receiver->get_id() <<std::endl;
        }
    }
}

std::string generate_structure_report(const Factory& factory){
    std::stringstream out_stream;
    //RAMPS SECTION
    out_stream << std::endl;
    out_stream << "== LOADING_RAMPS ==" <<std::endl;
    out_stream << std::endl;
    for(const auto& ramp : factory.get_ramps()){
        out_stream << "LOADING_RAMP #" << ramp.get_id() << std::endl;
        out_stream << "  Delivery interval: " << ramp.get_period() << std::endl;
        out_stream << "  Receivers:" << std::endl;
        for(const auto& p_receiver : ramp.get_receivers()){
            out_stream << "    " << to_string(p_receiver->get_receiver_type()) << " #" <<p_receiver->get_id() << std::endl;
        }
        out_stream << std::endl;
    }
    //WORKERS SECTION
    out_stream << std::endl;
    out_stream << "== WORKERS ==" <<std::endl;
    out_stream << std::endl;
    for(const auto& worker : factory.get_workers()){
        out_stream << "WORKER #" << worker.get_id() << std::endl;
        out_stream << "  Processing time: " << worker.get_period() << std::endl;
        out_stream << "  Queue type: " << to_string(worker.get_queue_type()) << std::endl;
        out_stream << "  Receivers:" << std::endl;
        for(const auto& p_receiver : worker.get_receivers()){
            out_stream << "    " << (p_receiver->get_receiver_type() == ReceiverType::Worker ? "worker" : "storehouse")
            << " #" <<p_receiver->get_id() << std::endl;
        }
        out_stream << std::endl;
    }
    //STOREHOUSES SECTION
    out_stream << std::endl;
    out_stream << "== STOREHOUSES ==" <<std::endl;
    out_stream << std::endl;
    for(const auto& storehous : factory.get_storehouses()){
        out_stream << "STOREHOUSE #" << storehous.get_id() << std::endl;
        out_stream << std::endl;
    }
    return out_stream.str();
}

std::string genertate_simulation_turn_report(const Factory &factory){
    std::stringstream out_stream;
    //WORKERS SECTION
    out_stream << std::endl;
    out_stream << "== WORKERS ==" <<std::endl;
    out_stream << std::endl;
    for(const auto& worker : factory.get_workers()){
        out_stream << "WORKER #" << worker.get_id() << std::endl;
        out_stream << "  Queue:";
        Time processing_progress = worker.get_processing_progress();
        if(processing_progress > 0){
            out_stream << " #" << worker.get_processed_package().get_id() <<
            " (pt = " << processing_progress << ")";
            for(const auto& package : worker.get_packages()){
                out_stream << ", #" << package.get_id();
            }
        }
        out_stream << std::endl;
        out_stream << std::endl;
    }
    //STOREHOUSES SECTION
    out_stream << std::endl;
    out_stream << "== STOREHOUSES ==" <<std::endl;
    out_stream << std::endl;
    for(const auto& storehouse : factory.get_storehouses()){
        out_stream << "STOREHOUSE #" << storehouse.get_id() << std::endl;
        out_stream << "  Queue:";
        const auto packages = storehouse.get_packages();
        auto it = packages.cbegin();
        if( it != packages.cend()) {
            out_stream << " #" << it->get_id();
            while(++it!= packages.cend()){
                out_stream << ", #" << it->get_id();
            }
        }
        out_stream << std::endl;
        out_stream << std::endl;
    }
    return out_stream.str();
}

// 4b: Wrześniak (297474), Twardosz (297466)