// 4b: Wrześniak (297474), Twardosz (297466)

#ifndef RECEIVERPREFERENCES_LIBRARY_HPP
#define RECEIVERPREFERENCES_LIBRARY_HPP

#include "Types.hpp"
#include "IPackageReceiver.hpp"

#include <map>
#include <vector>
#include <random>

extern ProbabilityGenerator DEFAULT_PROBABILITY_GENERATOR;

class ReceiverPreferences{
public:
    ReceiverPreferences(ProbabilityGenerator generator)
    : _generator(generator){}
    ReceiverPreferences(const ReceiverPreferences& receiver_preferences)
    : _generator(receiver_preferences._generator){}
    void add_receiver(IPackageReceiver* p_receiver);
    void remove_receiver(IPackageReceiver* p_receiver);
    std::vector<IPackageReceiver*> get_receivers()const;
    bool contains(IPackageReceiver* p_receiver)const;
	IPackageReceiver* ger_rand_receiver()const;
private:
	void probability_scale();
private:
	ProbabilityGenerator _generator;
	std::map<IPackageReceiver*, double> _receiver_preferences;
};

#endif

// 4b: Wrześniak (297474), Twardosz (297466)