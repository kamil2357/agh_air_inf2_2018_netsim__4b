// 4b: Wrześniak (297474), Twardosz (297466)

#ifndef PACKAGE_LIBRARY_HPP
#define PACKAGE_LIBRARY_HPP

#include "Types.hpp"

class Package{
public:
	Package()
	:_id(_next_id++){};
	ElementID get_id()const;
	static void reset_packaged_id(){
		_next_id = 1;
	}
private:
	ElementID _id;
	static ElementID _next_id;
};

#endif

// 4b: Wrześniak (297474), Twardosz (297466)