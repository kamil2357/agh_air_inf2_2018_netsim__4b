// 4b: Wrześniak (297474), Twardosz (297466)

#ifndef SPECIFICTURNSREPORTNOTIFIER_LIBRARY_HPP
#define SPECIFICTURNSREPORTNOTIFIER_LIBRARY_HPP

#include "IReportNotifier.hpp"
#include "Types.hpp"

#include <vector>
#include <algorithm>

class SpecificTurnsReportNotifier : public IReportNotifier {
public:
	SpecificTurnsReportNotifier(const std::vector<Time>& turns_of_rep)
		:_turns_of_rep(turns_of_rep){}
	bool should_generate_report(Time time) override{
		return std::find(_turns_of_rep.cbegin(), _turns_of_rep.cend(), time) != _turns_of_rep.cend();
	}
private:
	std::vector<Time> _turns_of_rep;
};

#endif

// 4b: Wrześniak (297474), Twardosz (297466)