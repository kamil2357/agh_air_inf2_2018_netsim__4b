// 4b: Wrześniak (297474), Twardosz (297466)

#include "Factory.hpp"

#include <string>
#include <fstream>


Factory load_factory_structure(std::istream& in);
void save_factory_structure(std::ostream& out, const Factory& factory);
std::string generate_structure_report(const Factory& factory);
std::string genertate_simulation_turn_report(const Factory &factory);
// 4b: Wrześniak (297474), Twardosz (297466)