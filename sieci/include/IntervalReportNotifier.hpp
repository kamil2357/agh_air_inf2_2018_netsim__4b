// 4b: Wrześniak (297474), Twardosz (297466)

#ifndef INTERVALREPORTNOTIFIER_LIBRARY_HPP
#define INTERVALREPORTNOTIFIER_LIBRARY_HPP

#include "Types.hpp"
#include "IReportNotifier.hpp"

class IntervalReportNotifier : public IReportNotifier{
public:
    IntervalReportNotifier(Time interval)
    	:_interval(interval){}
	bool should_generate_report(Time time) override{
		return (time - 1) % _interval == 0;
    }
private:
	Time _interval;
};

#endif

// 4b: Wrześniak (297474), Twardosz (297466)