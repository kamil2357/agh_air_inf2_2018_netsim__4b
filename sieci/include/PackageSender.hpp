// 4b: Wrześniak (297474), Twardosz (297466)

#ifndef PACKAGESENDER_LIBRARY_HPP
#define PACKAGESENDER_LIBRARY_HPP

#include "ReceiverPreferences.hpp"
#include "Package.hpp"

#include <memory>
#include <vector>

class PackageSender{
public:
	PackageSender(ProbabilityGenerator generator)
	:_receiver_preferences(generator){}
    void add_receiver(IPackageReceiver* p_receiver);
    void remove_receiver(IPackageReceiver* p_receiver);
	std::vector<IPackageReceiver*> get_receivers()const;
	virtual ~PackageSender() = default;
	bool is_receiver(IPackageReceiver* p_receiver)const ;
protected:
    void send_package(Package package);
public:
	ReceiverPreferences _receiver_preferences;
};

#endif

// 4b: Wrześniak (297474), Twardosz (297466)