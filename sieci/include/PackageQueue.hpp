// 4b: Wrześniak (297474), Twardosz (297466)

#ifndef PACKAGEQUEUE_LIBRARY_HPP
#define PACKAGEQUEUE_LIBRARY_HPP

#include "IPackageQueue.hpp"
#include "Package.hpp"

#include <deque>
#include <memory>
#include <vector>

class PackageQueue : public IPackageQueue{
public:
    PackageQueue(QueueType queue_type)
    :_queue_type(queue_type){}
	void add_package(Package) override;
	std::vector<Package> get_packages()const override;
	void remove_package(ElementID id) override;
	bool is_empty()const override;
	Package take_package()override;
	QueueType get_queue_type()const override;
	std::unique_ptr<IPackageQueue> clone()const override;
	std::unique_ptr<IPackageStockpile> clone_storehouse()const override;
private:
	std::deque<Package> _package_col;
	QueueType _queue_type;
};
#endif

// 4b: Wrześniak (297474), Twardosz (297466)