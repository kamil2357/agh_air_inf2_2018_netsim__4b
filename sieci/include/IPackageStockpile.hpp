// 4b: Wrześniak (297474), Twardosz (297466)

#ifndef IPACKAGESTOCKPILE_LIBRARY_HPP
#define IPACKAGESTOCKPILE_LIBRARY_HPP

#include "Package.hpp"
#include "Types.hpp"

#include <memory>
#include <vector>

class IPackageStockpile{
public:
    virtual void add_package(Package) = 0;
    virtual std::vector<Package> get_packages()const = 0;
    virtual ~IPackageStockpile() = default;
    virtual std::unique_ptr<IPackageStockpile> clone_storehouse()const=0;
};

#endif

// 4b: Wrześniak (297474), Twardosz (297466)