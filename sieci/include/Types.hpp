// 4b: Wrześniak (297474), Twardosz (297466)

#ifndef TYPES_LIBRARY_HPP
#define TYPES_LIBRARY_HPP

#include <functional>
#include <iterator>
#include <bits/forward_list.h>

using Time = unsigned int;
using TimeOffset = unsigned int;
using ElementID = unsigned int;
using ProbabilityGenerator = std::function<double()>;

#endif

// 4b: Wrześniak (297474), Twardosz (297466)