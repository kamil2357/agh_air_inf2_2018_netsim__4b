// 4b: Wrześniak (297474), Twardosz (297466)

#ifndef NODECOLLECTION_LIBRARY_HPP
#define NODECOLLECTION_LIBRARY_HPP

#include "Ramp.hpp"
#include "Worker.hpp"
#include "Storehouse.hpp"

#include <algorithm>
#include <memory>
#include <list>

template<typename T>
class NodeCollection{
public:
    void add_node(T&& elem);
    void remove_node(ElementID id);
    const std::list<T>& get_elements() const{
        return _collection;
    }
    T& get_element(ElementID id){
        return *find(id);
    }
    typename std::list<T>::const_iterator cbegin() const{
        return _collection.cbegin();
    }
    typename std::list<T>::const_iterator begin() const{
        return _collection.cbegin();
    }
    typename std::list<T>::iterator begin(){
        return _collection.begin();
    }
    typename std::list<T>::const_iterator cend() const{
        return _collection.cend();
    }
    typename std::list<T>::const_iterator end() const{
        return _collection.cend();
    }
    typename std::list<T>::iterator end() {
        return _collection.end();
    }
private:
    typename std::list<T>::iterator find(ElementID id);
private:
    std::list<T> _collection;
};

template<typename T>
void NodeCollection<T>::add_node(T&& elem){
    const auto it = std::lower_bound(_collection.cbegin(), _collection.cend(), elem,
                                     [](const auto& e1, const auto& e2){ return e1.get_id() < e2.get_id();});
    if(it != _collection.end() && it->get_id() == elem.get_id()){
        throw std::runtime_error("Proba dodania elementu o istniejacym id = " + std::to_string(elem.get_id()));
    }
    _collection.emplace(it, std::move(elem));
}

template<typename T>
void NodeCollection<T>::remove_node(ElementID id){
    auto it = find(id);
    _collection.erase(it);
}

template<typename T>
typename std::list<T>::iterator NodeCollection<T>::find(ElementID id){
    auto it = std::find_if(_collection.begin(), _collection.end(), [id](const auto& e){return e.get_id() == id;});
    if(it == _collection.end()){
        throw std::runtime_error("Element o podanym id nie istnieje");
    }
    return it;
}

using Ramps=NodeCollection<Ramp>;
using Workers=NodeCollection<Worker>;
using Storehouses=NodeCollection<Storehouse>;

#endif

// 4b: Wrześniak (297474), Twardosz (297466)