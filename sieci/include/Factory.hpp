// 4b: Wrześniak (297474), Twardosz (297466)

#ifndef FACTORY_LIBRARY_HPP
#define FACTORY_LIBRARY_HPP

#include "NodeCollection.hpp"
#include "Types.hpp"
#include "IReportNotifier.hpp"

#include <fstream>

class Factory{
public:
	Factory() = default;
	Factory(const Factory& src);
    void update();
    void check_structure() const;
	void add_ramp(Ramp ramp);
	void add_worker(Worker worker);
	void add_storehouse(Storehouse storehouse);
    const std::list<Ramp>& get_ramps() const{
		return _ramps.get_elements();
	}
	const std::list<Worker>& get_workers() const{
		return _workers.get_elements();
	}
	const std::list<Storehouse>& get_storehouses() const{
		return _storehouses.get_elements();
	}
	Ramp& get_ramp(ElementID id);
	Worker& get_worker(ElementID id);
	Storehouse& get_storehouse(ElementID id);
	void create_ramp_worker_link(ElementID ramp_id, ElementID worker_id);
	void create_worker_worker_link(ElementID worker1_id, ElementID worker2_id);
	void create_worker_storehouse_link(ElementID worker_id, ElementID storehose_id);
	void remove_ramp(ElementID id);
	void remove_worker(ElementID id);
	void remove_storehouse(ElementID id);
private:
	Ramps _ramps;
	Workers _workers;
	Storehouses _storehouses;
};

void simulate(Factory factory, Time simulation_time, std::ostream& out,
        std::unique_ptr<IReportNotifier> p_report_notyfier);

#endif

// 4b: Wrześniak (297474), Twardosz (297466)