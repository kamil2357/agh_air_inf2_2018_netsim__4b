// 4b: Wrześniak (297474), Twardosz (297466)

#ifndef IREPORTNOTIFIER_LIBRARY_HPP
#define IREPORTNOTIFIER_LIBRARY_HPP

#include "Types.hpp"

class IReportNotifier{
public:
    virtual bool should_generate_report(Time time) = 0;
	virtual ~IReportNotifier() = default;
};

#endif

// 4b: Wrześniak (297474), Twardosz (297466)