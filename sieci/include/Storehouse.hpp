// 4b: Wrześniak (297474), Twardosz (297466)

#ifndef STOREHOUSE_LIBRARY_HPP
#define STOREHOUSE_LIBRARY_HPP

#include "IPackageQueue.hpp"
#include "Package.hpp"
#include "IPackageReceiver.hpp"

#include <memory>
#include <vector>

class Storehouse : public IPackageReceiver{
public:
    Storehouse(ElementID id,std::unique_ptr<IPackageStockpile> queue)
    : _id(id),_products(std::move(queue)){}
    Storehouse(const Storehouse &storehouse)
    :_id(storehouse._id),_products(storehouse._products->clone_storehouse()){}
	void take_package(Package package) override;
	std::vector<Package> get_packages()const override;
	ElementID get_id()const override;
	ReceiverType get_receiver_type()const override;
private:
	ElementID _id;
	std::unique_ptr<IPackageStockpile> _products;
	ReceiverType _receiver_type=ReceiverType::Storehouse;
};

#endif

// 4b: Wrześniak (297474), Twardosz (297466)