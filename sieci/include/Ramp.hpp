// 4b: Wrześniak (297474), Twardosz (297466)

#ifndef RAMP_LIBRARY_LIBRARY_HPP
#define RAMP_LIBRARY_LIBRARY_HPP

#include "Types.hpp"
#include "PackageSender.hpp"

#include <optional>

class Ramp : public PackageSender{
public:
    Ramp(ElementID id,Time period,ProbabilityGenerator generator=DEFAULT_PROBABILITY_GENERATOR)
    	:PackageSender(generator),_id(id),_period(period), _from_last_delivery(period-1){};
	void delivery_processing();
	void give_if_finished();
	ElementID get_id()const;
	Time get_period()const;
private:
	ElementID _id;
	Time _period;
	TimeOffset _from_last_delivery = 0;
	std::optional<Package> _hold_package;
};

#endif

// 4b: Wrześniak (297474), Twardosz (297466)