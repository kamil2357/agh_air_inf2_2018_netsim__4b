// 4b: Wrześniak (297474), Twardosz (297466)

#ifndef IPACKAGEQUEUE_LIBRARY_HPP
#define IPACKAGEQUEUE_LIBRARY_HPP

#include "IPackageStockpile.hpp"

enum class QueueType {
	FIFO,
	LIFO
};

class IPackageQueue : public IPackageStockpile{
public:
    virtual QueueType get_queue_type()const=0;
	virtual void remove_package(ElementID id) = 0;
    virtual ~IPackageQueue() = default;
    virtual bool is_empty()const = 0;
    virtual Package take_package()=0;
    virtual void add_package(Package)=0;
	virtual std::unique_ptr<IPackageQueue> clone()const=0;
	virtual std::unique_ptr<IPackageStockpile> clone_storehouse()const=0;
};

#endif

// 4b: Wrześniak (297474), Twardosz (297466)