// 4b: Wrześniak (297474), Twardosz (297466)

#ifndef IPACKAGERECEIVER_LIBRARY_HPP
#define IPACKAGERECEIVER_LIBRARY_HPP

#include "Types.hpp"
#include "Package.hpp"

#include <memory>
#include <vector>

enum class ReceiverType {
	Storehouse,
	Worker
};

class IPackageReceiver{
public:
	virtual ReceiverType get_receiver_type()const=0;
    virtual void take_package(Package package) = 0;
	virtual std::vector<Package> get_packages()const = 0;
	virtual ~IPackageReceiver() = default;
	virtual ElementID get_id()const=0;

};
#endif

// 4b: Wrześniak (297474), Twardosz (297466)