// 4b: Wrześniak (297474), Twardosz (297466)

#ifndef WORKER_LIBRARY_HPP
#define WORKER_LIBRARY_HPP

#include "IPackageReceiver.hpp"
#include "PackageSender.hpp"
#include "PackageQueue.hpp"
#include "Types.hpp"
#include "Package.hpp"

#include <memory>
#include <optional>
#include <vector>

class Worker : public IPackageReceiver, public PackageSender{
public:
    Worker(ElementID id, Time period, std::unique_ptr<PackageQueue> queue,
    		ProbabilityGenerator generator=DEFAULT_PROBABILITY_GENERATOR)
        :PackageSender(generator),_id(id),_period(period),_packages(std::move(queue)){};
    Worker(const Worker &worker)
    	:PackageSender(PackageSender(worker)),_id(worker._id),_period(worker._period),_packages(worker._packages->clone()),
   		 _processed_package(worker._processed_package),_processing_progress(worker._processing_progress){}
	void process_package();
	void send_if_finished();
	void process_new_package();
	void take_package(Package package) override;
	std::vector<Package> get_packages()const override;
	ReceiverType get_receiver_type()const override;
	ElementID get_id()const override;
	Time get_period()const;
	QueueType get_queue_type() const;
	Package get_processed_package() const;
	TimeOffset get_processing_progress() const;
private:
    ElementID _id;
    Time _period;
    std::unique_ptr<IPackageQueue> _packages;
    ReceiverType _receiver_type=ReceiverType::Worker;
	std::optional<Package> _processed_package;
	TimeOffset _processing_progress = 0;
};

#endif

// 4b: Wrześniak (297474), Twardosz (297466)